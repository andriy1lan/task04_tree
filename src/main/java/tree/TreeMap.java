package tree;

public class TreeMap {

    Node root;
    int size;
    public TreeMap() {
        size = 0;
        root = null;

    }

    public Object put(int key, Object val) {

        root = put(root, key, val);
        size++;
        return val;
    }

    private Node put(Node root, int key, Object val) {
        if (root == null)
            return new Node(key, val);

        if (key < root.key) {
            root.left = put(root.left, key, val);
            root.left.parent=root;
        }
        else if (key > root.key) {
            root.right = put(root.right, key, val);
            root.right.parent=root;
        }
        else
            root.value = val;
        return root;
    }


    void delete(int key)
    {
        root = delete(root, key);
        size--;
    }

    Node delete(Node root, int key)
    {
        if (root == null)  return root;

        if (key < root.key) {
            root.left = delete(root.left, key);
            root.left.parent=root; }

        else if (key > root.key)  {
            root.right = delete(root.right, key);
            root.right.parent=root;  }

        else
        {
            if (root.left == null)
                return root.right;
            else if (root.right == null)
                return root.left;
            root.key = minimalValue(root.right);
            root.right = delete(root.right, root.key);
        }
        return root;
    }

    int minimalValue(Node root) {
        int min = root.key;
        while (root.left != null)
        {
            min = root.left.key;
            root = root.left;
        }
        return min;
    }

    public void clear () {
        this.size = 0;
        this.root=null;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        if (size == 0)
            return true;
        return false;
    }


    public boolean containKey (int key)  {
        Node current = root;
        while (current != null) {
            if (key < current.key) {
                current = current.left;
            } else if (key > current.key) {
                current = current.right;
            } else {
                return true;
            }
        }
        return false;

    }

    public Object get (int key)
    {
        Node current = root;
        while (current != null) {
            if (key < current.key) {
                current = current.left;
            } else if (key > current.key) {
                current = current.right;
            } else {
                return current.value;
            }
        }
        return null;
    }

    public void printMap(Node root1) {
        if (root1 != null) {
            printMap(root1.left);
            if (root1.parent==null) System.out.print(root1.key+"*" + root1.value + "--");
            else System.out.print(root1.key+"*" + root1.value+ "-" + root1.parent.key + "--");
            // if (root1.left!=null )  System.out.print(root1.left.key+"L---");
            // if (root1.right!=null) System.out.print(root1.right.key+"R---");
            printMap(root1.right);
        }
    }

    public static void main(String[] args)

    {
        TreeMap tm=new TreeMap();
        tm.put(3,"b");
        tm.put(2,"c");
        tm.put(7,"d");
        tm.put(9,"a");
        tm.put(8,"e");
        tm.put(15,"f");
        tm.printMap(tm.root);
        System.out.println();
        System.out.println(tm.root.key);
        System.out.println(tm.get(2));
        tm.delete(9);
        System.out.println(tm.containKey(9));
        tm.printMap(tm.root);
        System.out.println();
        tm.printMap(tm.root);
    }
}