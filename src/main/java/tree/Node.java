package tree;

public class Node {
    int key;
    Object value;
    Node left;
    Node right;
    Node parent;

    public Node(int key, Object value) {
        this.key = key;
        this.value = value;
    }
}